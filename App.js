/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Button, Text, View} from 'react-native';
// import { createStackNavigator, createAppContainer, createBottomTabNavigator } from "react-navigation";
import LoginView from "./app/views/LoginView";
import { createRootNavigator } from './app/router';
import firebase from 'react-native-firebase';
import { AsyncStorage,YellowBox, Alert } from 'react-native';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
/*
// NAVIGATION PART
const RootStack = createBottomTabNavigator( {
  Descubre: ShopsListScreen,
  Suscripcion: SuscriptionScreen,
},
{
  initialRouteName: 'Descubre',
},
{


},);

const AppContainer = createAppContainer(RootStack);
*/
export default class App extends Component<Props> {
  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }
  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body, data } = notification;
        this.showAlert(title, body, data);
    });
  
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { title, body, data } = notificationOpen.notification;
        this.showAlert(title, body, data);
    });
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body, data } = notificationOpen.notification;
        this.showAlert(title, body, data);
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }
  
  showAlert(title, body, data) {
    this.setNotification(data);
    console.log('title: ', title);
    console.log('body', body + data);
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }
  setNotification(data) {
    AsyncStorage.getItem('notifications', (err, result) => {
      const notificationList = result ? JSON.parse(result): [];
        notificationList.push(data);
        if (notificationList.length > 3) {
          notificationList.shift();
        }
        AsyncStorage.setItem('notifications', JSON.stringify(notificationList));
    });
  }
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            // user has a device token
            await AsyncStorage.setItem('fcmToken', fcmToken);
        }
    }
  }
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.log('permission rejected');
    }
  }

  render() {
    console.disableYellowBox= true;
    YellowBox.ignoreWarnings(['Warning: ...']);
    const FirstView = createRootNavigator(false);
    return(
      <FirstView/>
    );
    }
}