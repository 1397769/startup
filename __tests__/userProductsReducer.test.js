import reducer from'../app/redux/reducers/userProductsReducer'
import {  SET_USER_PRODUCTS, ADD_USER_PRODUCT, DELETE_USER_PRODUCT } from "../app/redux/actions/actionTypes";

import { types } from '@babel/core';
describe('userProduct reducer',()=>{
    it('should return the initial state', ()=> {
        expect(reducer(undefined,{})).toEqual(
            {
                userProducts: []
            })
    })
    it('should handle SET_USER_PRODUCTS', ()=> {
        expect(reducer([],{
            type: SET_USER_PRODUCTS,
            userProducts: ['test1','test2','test3']
        })).toEqual(
            {
                userProducts: ['test1','test2','test3']
            })
        expect(reducer({userProducts: ['error1','error2','error3']},{
                type: SET_USER_PRODUCTS,
                userProducts: ['test1','test2','test3']
        })).toEqual(
            {
                userProducts: ['test1','test2','test3']
            })
    })
})

describe('userProduct ADD_USER_PRODUCT',()=>{
    it('should handle ADD_USER_PRODUCT with not data', ()=> {
        expect(reducer
            ([],{
            type: ADD_USER_PRODUCT,
            userProduct: 'data1'
        }
        )).toEqual(
            {
                userProducts: 'data1'
            })
        expect(reducer({userProducts: ['data1','data2','data3']},{
                type: ADD_USER_PRODUCT,
                userProduct: 'addData'
        })).toEqual(
            {
                userProducts: ['data1','data2','data3', 'addData']
            })
    })
})
describe('userProduct DELETE_USER_PRODUCT',()=>{
    it('should handle DELETE_USER_PRODUCT with not data', ()=> {
        expect(reducer
            ({userProducts: [{data1: 'data1'}]},{
            type: DELETE_USER_PRODUCT,
            idProduct: 'data1'
        }
        )).toEqual(
            {
                userProducts: []
            })
        expect(reducer({userProducts: [{data1: 'data1'},{data2: 'data2'},{data3: 'data3'}]},{
                type: DELETE_USER_PRODUCT,
                idProduct: 'data2'
        })).toEqual(
            {
                userProducts: [{data1: 'data1'},{data3: 'data3'}]
            })
    })
})