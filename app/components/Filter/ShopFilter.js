import React from 'react';
import {
    View, Picker,
    FlatList,
    StyleSheet
} from 'react-native';
import ShopItem from '../ShopItem/ShopItem'
import SwitchSelector from "react-native-switch-selector";
import Slider from '@react-native-community/slider';



class ShopFilter extends React.Component {
    render() {
        const options = [
            { label: "Hombre", value: "h" },
            { label: "Mujer", value: "m" }
        ];

        return (
            <View>
                <SwitchSelector
                    onPrectedColor={colors.white}
                    ess={value => this.setState({ gender: value })}
                    selbuttonColor={colors.purple}
                    borderColor={colors.purple}
                    hasPadding
                />
                <Picker
                    style={{ width: 100 }}
                    onValueChange={(type) => this.setState({ typeProduct: type })}>
                    <Picker.Item label="Camisa" value="java" />
                    <Picker.Item label="Pantalón" value="js" />
                </Picker>
                <Slider
                    style={{width: 200, height: 40}}
                    minimumValue={1}
                    maximumValue={200}
                    value={5}
                    minimumTrackTintColor="#FFFFFF"
                    maximumTrackTintColor="#000000"
                />
                <Slider
                    style={{width: 200, height: 40}}
                    minimumValue={1}
                    maximumValue={5}
                    value={0.2}
                    minimumTrackTintColor="#FFFFFF"
                    maximumTrackTintColor="#000000"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listContainer: {
        width: "100%"
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    }
});

export default ShopList;
