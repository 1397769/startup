import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import ButtonForm from "../UI/ButtonForm/ButtonForm";

const SCREEN_WIDTH = Dimensions.get('window').width;
const fullWidth = SCREEN_WIDTH;
const halfWidth = 0.95 * SCREEN_WIDTH / 2;
const fullImageWidth = fullWidth/2;

// aspect ratio
const ratio = 17 / 11;
const ratioImage = 14 / 14;
class NotificationItem extends React.Component {
  render() {
    var priceField = null;
    var discountPrice = null;
    var buttonSubscribe = null;
    var productValue = this.props.productValue;
    if (productValue.dto && parseFloat(productValue.dto) > 0) {
      priceField = <Text style={styles.oldPrice}>
        {'Antes ' + parseFloat(productValue.price).toFixed(2) + ' €'}
      </Text>;
      var bestPrice = parseFloat(productValue.price) * (1-(parseFloat(productValue.dto) / 100));
      discountPrice = <Text style={styles.bestPrice}>
        {'AHORA '+ parseFloat(bestPrice).toFixed(2) + ' €'}
      </Text>;

    } else {
      priceField = <Text style={styles.aPrice}>
        {'Precio actual ' + parseFloat(productValue.price).toFixed(2) + ' €'}
      </Text>;

    }
    return (
      <View>
        <View style={styles.listItem}>
        <TouchableOpacity onPress={this.props.onItemPressed}>
          <Image resizeMode="cover" source={{ uri: productValue.imag }} style={styles.largeImage} />
          </TouchableOpacity>
          <View>
          <Text style={styles.headerItem}>
              {productValue.idBrand}
            </Text>
            <Text style={styles.headerItem}>
              {productValue.name}
            </Text>
            <Text style={styles.headerItem}>
             {discountPrice}
            </Text>
            <View>
            <Text>
              {priceField}
            </Text>
            </View>
          </View>
        </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  listItem: {
    width: "100%",
    marginBottom: 5,
    padding: 10,
    backgroundColor: "#e8e5e5",
    flexDirection: "row",
    alignItems: "center"
    /*
    backgroundColor: "white",
    flexDirection: "column",
   // alignItems: "flex-end",
    height: fullImageWidth * ratio + fullImageWidth*0.2,
    width: '100%',
    margin: halfWidth * 0.0205,*/
  },
  largeImage: {
    marginRight: 5,
    height: fullImageWidth*ratioImage,
    width: fullImageWidth,

    /*
    height: fullImageWidth * ratioImage * 0.97,
    width: fullImageWidth*/
  },
  headerItem: {
    textTransform: 'uppercase',
      textAlign: 'left',
      fontFamily: 'Cochin',
      width: '100%',
      fontWeight: 'bold',
      fontSize: 12
  },
  oldPrice: {
    textAlign: 'left',
    // textDecorationLine: 'line-through',
    // textDecorationStyle: 'solid',
  },
  aPrice: {
    textAlign: 'left',
  },
  bestPrice: {
    textAlign: 'left',
    color: '#ba0b37',
  },
  separator: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
});
export default NotificationItem;
