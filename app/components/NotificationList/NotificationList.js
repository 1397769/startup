import React from 'react';
import { View,
  FlatList,
  StyleSheet } from 'react-native';
  import NotificationItem from "../NotificationItem/NotificationItem";

  class NotificationList extends React.Component {
  render() {
    return(
    <FlatList
    style={styles.listContainer}
    data={this.props.products}
    renderItem={info => (
      <NotificationItem
        productValue= {info.item}
        onSubscribePressed = {()=> this.props.onSubscribeSelected(info.item)}
        onItemPressed={() => this.props.onItemSelected(info.item)}
      />
    )}
    keyExtractor={(item, index) => index.toString()}
  />
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    width: "100%",
}
});

export default NotificationList;
