import React from 'react';
import { View,
  FlatList,
  StyleSheet } from 'react-native';
  import ProductItem from "../ProductItem/ProductItem";

  class ProductList extends React.Component {
  render() {
    return(
    <FlatList
    extraData={this.props.extraData}
    style={styles.listContainer}
    data={this.props.products}
    numColumns={2}
    renderItem={info => (
      <ProductItem
        productValue= {info.item}
        onSubscribePressed = {()=> this.props.onSubscribeSelected(info.item)}
        onItemPressed={() => this.props.onItemSelected(info.item)}
      />
    )}
    keyExtractor={(item, index) => index.toString()}
  />
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    width: "100%",
}
});

export default ProductList;
