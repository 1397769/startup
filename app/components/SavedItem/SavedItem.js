import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import ButtonSave from "../UI/ButtonSave/ButtonSave";

const SCREEN_WIDTH = Dimensions.get('window').width;
const fullWidth = SCREEN_WIDTH;
const halfWidth = 0.95 * SCREEN_WIDTH / 2;
const fullImageWidth = fullWidth;

// aspect ratio
const ratio = 17 / 11;
const ratioImage = 16 / 10;
class SavedItem extends React.Component {
  render() {
    var priceField = null;
    var discountPrice = null;
    var buttonSubscribe = null;
    var productValue = this.props.productValue;
    if (productValue.dto && productValue.dto > 0) {
      priceField = <Text style={styles.oldPrice}>
        {parseFloat(productValue.price).toFixed(2) + ' €'}
      </Text>;
      var bestPrice = productValue.price * (1-(productValue.dto / 100));
      discountPrice = <Text style={styles.bestPrice}>
        {'   '+parseFloat(bestPrice).toFixed(2) + ' €'}
      </Text>;

    } else {
      priceField = <Text style={styles.aPrice}>
        {parseFloat(productValue.price).toFixed(2) + ' €'}
      </Text>;

    }
    buttonSubscribe = <ButtonSave isSubs={productValue.isSubs}  onPress={this.props.onSubscribePressed}>Sub</ButtonSave>
    return (
      <View>
        <View style={styles.listItem}>
        <TouchableOpacity onPress={this.props.onItemPressed}>
          <Image resizeMode="cover" source={{ uri: productValue.imag }} style={styles.largeImage} />
          </TouchableOpacity>
          <View>
            <Text style={styles.headerItem}>
              {productValue.idBrand}
            </Text>
            <Text style={styles.headerItem}>
              {productValue.name}
            </Text>
            <View style= {{flexDirection:'row',justifyContent: 'space-between'}}>
            <Text>
            {priceField}
            {discountPrice}
            </Text>
            {buttonSubscribe}
            </View>
          </View>
        </View>
        <View style={styles.separator} />
        </View>
    );
  }
}
const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
    alignItems: "center",
    height: fullImageWidth * ratio + fullImageWidth*0.2,
    width: fullImageWidth,
    margin: halfWidth * 0.0205,
  },
  largeImage: {
    height: fullImageWidth * ratioImage * 0.97,
    width: fullImageWidth
  },
  headerItem: {
    textTransform: 'uppercase',
    textAlign: 'left',
    fontFamily: 'Cochin',
    width: fullImageWidth,
    fontWeight: 'bold',
  },
  oldPrice: {
    textAlign: 'left',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  aPrice: {
    textAlign: 'left',
  },
  bestPrice: {
    textAlign: 'left',
    color: '#ba0b37',
  },
  separator: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
});
export default SavedItem;
