import React from 'react';
import { View, Text,
  FlatList,
  StyleSheet } from 'react-native';
  import SavedItem from "../SavedItem/SavedItem";
  import ButtonFilter from "../UI/ButtonFilter/ButtonFilter";


  class SavedList extends React.Component {
    logout = () => {
      return(     <ButtonFilter color="#bf053d" onPress={this.props.logOut}>
      Cerrar sesión
    </ButtonFilter>);
    }
  render() {
    return(
    <FlatList
    style={styles.listContainer}
    data={this.props.products}
    numColumns={1}
    renderItem={info => (
      <SavedItem
        productValue= {info.item}
        onSubscribePressed = {()=> this.props.onSubscribeSelected(info.item)}
        onItemPressed={() => this.props.onItemSelected(info.item)}
      />
    )}
    keyExtractor={(item, index) => index.toString()}
    ListFooterComponent={this.logout}
  />
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    width: "100%",
}
});

export default SavedList;
