import React from 'react';
import { View,
  Text,
  StyleSheet,
  Image, 
  Dimensions,
  TouchableOpacity } from 'react-native';
const SCREEN_WIDTH = Dimensions.get('window').width;
const fullImageWidth = SCREEN_WIDTH/3;

// aspect ratio
const ratio = 17 / 11;
const ratioImage = 14 / 14;
class ShopItem extends React.Component {
  render () {
    return(
      <TouchableOpacity onPress={this.props.onItemPressed}>
      <View style={styles.listItem}>
        <Image resizeMode="cover" source={{uri: this.props.shopImage}} style={styles.shopImage} />
        <View>
          <Text style = {styles.headerItem}>
            {this.props.shopName}
            </Text>
            <Text>
            {this.props.street + '\n' + this.props.nPromo + ' promociones'+ '\n' + this.props.timetable}
          </Text>
        </View>
      </View>
      <View style={styles.separator} />
    </TouchableOpacity>
    );
  }
}
  const styles = StyleSheet.create({
    listItem: {
      width: "100%",
      marginBottom: 5,
      padding: 10,
      backgroundColor: "white",
      flexDirection: "row",
      alignItems: "center"
    },
    shopImage: {
        marginRight: 8,
        height: fullImageWidth*ratioImage ,
        width: fullImageWidth,
      },
    headerItem: {
      textTransform: 'uppercase',
      textAlign: 'left',
      fontFamily: 'Cochin',
      width: '100%',
      fontWeight: 'bold',
    },
    separator: {
      borderBottomColor: 'grey',
      borderBottomWidth: 1,
    },
  });
  export default ShopItem;
