import React from 'react';
import { View,
  FlatList,
  StyleSheet } from 'react-native';
import ShopItem from '../ShopItem/ShopItem'

  class ShopList extends React.Component {
  render() {
    return(
    <FlatList
    style={styles.listContainer}
    data={this.props.shops}
    renderItem={(info) => (
      <ShopItem
        shopName={info.item.name}
        shopImage={info.item.img}
        street={info.item.street}
        nPromo={info.item.nPromo}
        timetable={info.item.timetable}
        onItemPressed={() => this.props.onItemSelected(info.item)}
      />
    )}
    keyExtractor={(item, index) => index.toString()}
  />
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    width: "100%"
  }
});

export default ShopList;
