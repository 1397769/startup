import React, { Component } from 'react';
import {
  TouchableOpacity,
  TouchableNativeFeedback,
  Text,
  View,
  StyleSheet,
  Platform
} from "react-native";
import { conditionalExpression } from '@babel/types';

const buttonForm = props => {
  const content = (
    <View style={[styles.button, { backgroundColor: props.color },
    props.disabled ? styles.disabled : null
    ]}>
      <Text style={props.disabled ? styles.disabledText : styles.text}>
        {props.children}
      </Text>
    </View>
  );
  if(props.disabled) {
    return content;
  }
  if (Platform.OS === "android") {
    <TouchableNativeFeedback onPress={props.onPress}>
      {content}
    </TouchableNativeFeedback>
  } else {
    <TouchableOpacity onPress={props.onPress}>
      {content}
    </TouchableOpacity>
  }
  return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>;

};

const styles = StyleSheet.create({
  button: {
    width: "100%",
    paddingVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(255, 255, 255, 0.7)",
    borderRadius: 4,
    marginBottom:30,
  },
  disabled: {
    backgroundColor: "#eee",
    borderColor: "#aaa"
  },
  disabledText: {
    color: "#aaa"
  },
  text: {
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 17,
    fontWeight: "bold",
  }
});

export default buttonForm;
