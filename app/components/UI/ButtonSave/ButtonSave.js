import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { conditionalExpression } from '@babel/types';
const windowW= Dimensions.get('window').width
const windowH = Dimensions.get('window').height

const buttonSave = props => {
  const content = (
    <View style={[styles.button, 
    props.isSubs ? styles.isSubs : styles.notSubs
    ]}>
    <TouchableOpacity
    onPress={props.onPress}>
      <Text style={props.isSubs ? styles.isSubsText : styles.text}>
        {props.children}
      </Text>
      </TouchableOpacity>
    </View>
  );
  
  return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>;

};

const styles = StyleSheet.create({
  button: {
    width: windowW*0.17,
    paddingVertical: 2,
    paddingLeft: 12,
    paddingRight: 14,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(255, 255, 255, 0.7)",
    borderRadius: 4,
    marginBottom:30,
  },
  isSubs: {
    backgroundColor: "#aaa",
    borderColor: "#bf053d"
  },
  notSubs: {
    backgroundColor: "#aaa",
    borderColor: "#aaa"
  },
  isSubsText: {
    color: "#ffffff",
    fontSize: 17,
    fontWeight: "bold",
  },
  text: {
    color: '#ffffff',
    fontSize: 17,
    fontWeight: "bold",
  }
});

export default buttonSave;
