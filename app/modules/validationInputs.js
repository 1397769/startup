
const validationInputs = (value, validation, compareTo) => {
    let isValid = true;
    for (let rule in validation) {
        switch(rule){
            case 'isEmail':
                isValid =isValid && validateEmail(value);
                break;
            case 'minLength':
                isValid = isValid && validateMinLength(value, validation[rule]);
                break;
            case 'equalTo':
                isValid = isValid && validateEqualTo(value, compareTo[rule]);
                break;
            case 'isEmpty':
                isValid = isValid && validateIsEmpy(value);
                break;
            default:
                isValid = true;
        }

    }
   return isValid;
};
const validateEmail = value => {
    return /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      value
    );
  };
  
  const validateMinLength = (value, minLength) => {
    return value.length >= minLength;
  };
  
  const validateEqualTo = (value, checkValue) => {
    return value === checkValue;
  };
  
  const validateIsEmpy = value => {
    return value.trim() !== "";
  };
  
  export default validationInputs;
  