import AsyncStorage from '@react-native-community/async-storage';
import { SET_TOKEN, DELETE_TOKEN } from "./actionTypes";
import { AUTH_NOT_NETWORK, INVALID_EMAIL } from "../../constants";
import firebase from "react-native-firebase";



const API_KEY = 'AIzaSyAKAzzSSTUr43tIi2-95xPNR0jYUyKEaXQ'
// Initialize Firebase
var config = {
    apiKey: "AIzaSyAKAzzSSTUr43tIi2-95xPNR0jYUyKEaXQ",
    authDomain: "testtfg-57399.firebaseapp.com",
    databaseURL: "https://testtfg-57399.firebaseio.com",
    storageBucket: "testtfg-57399.appspot.com",
  };
// firebase.initializeApp(config);
const getDeviceToken = async() => {
    const fcmToken = await AsyncStorage.getItem('fcmToken');
    return fcmToken;
  }
  
const sendDeviceToken = async(idToken) => {
    const deviceToken = await getDeviceToken();
    const tokenToSend = {
        [deviceToken]: true
    };
   return fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/notificationToken', 
        {
            method: "POST",
            body: JSON.stringify({
                notificationToken: tokenToSend
            }),
            headers: {
                Authorization: "Bearer " + idToken,
                "Content-Type": 'application/json'
            }
        }).catch(err => {
            alert('Error en el login');
        })
        .then(res => res.json())
        .then((shopsData)=> {
           return console.log('Datos recibidos: ', shopsData);
        });
}
export const authSignUp = (userAuthData) => {
//    return dispatch => {
        fetch('https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key='+ API_KEY, 
        {
            method: "POST",
            body: JSON.stringify({
                email: userAuthData.email,
                password: userAuthData.password,
                returnSecureToken: true
            }),
            headers: {
                "Content-Type": 'application/json'
            } 
        }).catch(err => {
            console.log(err);
            alert('Error en el proceso de autenticación');
        })
        .then(res => res.json())
        .then((valueData)=> {
            if(valueData.idToken) {
                alert('Registro completado con éxito');
            } else {
                alert('Error en el registro, intente de nuevo');
            }
            console.log(valueData);
        });

  //  }
}
export const authLogin = (userAuthData, props) => {
    return dispatch =>{
        // https://firebase.google.com/docs/auth/admin/verify-id-tokens
        console.log('Login module: ',userAuthData);
        firebase.auth().signInWithEmailAndPassword(userAuthData.email, userAuthData.password).then((aux)=> {
            firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
                if(idToken) {
                    dispatch(storeToken(idToken));
                    sendDeviceToken(idToken);
                }
              }).catch(function(error) {
                // Handle error
                console.log(error);
                alert('Email y/o contraseña incorrectos. Registrate para crear una cuenta');
              });
        }
            ).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            switch(errorCode) {
                case AUTH_NOT_NETWORK:
                    alert('No tienes internet. Comprueba la conexión');
                    break;
                case INVALID_EMAIL:
                    alert('Email no valido');
                    break;
                default:
                    alert('Email y/o contraseña incorrectos. Regístrate para crear una cuenta');
            }
          });

    };
};

export const storeToken = (idToken) => {
    return dispatch => {
        dispatch(setToken(idToken));
        AsyncStorage.setItem('auth:idToken', idToken);
    };
};

export const removeToken = () => {
    return {
        type: DELETE_TOKEN,
        idToken: '1'
    };
};

export const setToken = (idToken) => {
    return {
        type: SET_TOKEN,
        idToken: idToken
    };
};
export const deleteToken = () => {
        return async dispatch => {
            await AsyncStorage.clear();
            dispatch(removeToken());
            // await AsyncStorage.removeItem('auth:idToken');
            return true;
        }
}
