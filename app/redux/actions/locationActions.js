import React from 'react';
import { PermissionsAndroid,
Platform, ToastAndroid, BackHandler } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { SET_LOCATION } from "./actionTypes";
import { getShops } from './shopActions';
state = {
    loading: false,
    updatesEnabled: false,
    location: {}
  };
watchId = null;

export const hasLocationPermissionLocal = async () => {
    if (Platform.OS === 'ios' ||
        (Platform.OS === 'android' && Platform.Version < 23)) {
      return true;
    }
    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Persmio de localización necesario', ToastAndroid.LONG);
      BackHandler.exitApp();
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Permisos de localización anulados. Vuelva a instalar la app', ToastAndroid.LONG);
      BackHandler.exitApp();
    }

    return false;
  };

  export const getLocation = ()=>{ 
    return async dispatch => {
    const hasLocationPermission = await hasLocationPermissionLocal();

    if (!hasLocationPermission) return;

      Geolocation.getCurrentPosition(
        (position) => {
          dispatch(setLocation(position));
        },
        (error) => {
          console.log(error);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50 }
      );
  };
  }
 export const getLocationUpdates = (filter) =>{
  return async dispatch => {
    const hasLocationPermission = await hasLocationPermissionLocal();
    if (!hasLocationPermission) return;
      this.watchId = Geolocation.watchPosition(
        (position) => {
          filter.position = position
          dispatch(setLocation(position));
          dispatch(getShops(filter));

        },
        (error) => {
          dispatch(setLocation(error));
          console.log(error);
        },
        { enableHighAccuracy: true, distanceFilter: 20, interval: 5000, fastestInterval: 2000 }
      );
  }
 }
export const removeLocationUpdates = () => {
      if (this.watchId !== null) {
          Geolocation.clearWatch(this.watchId);
          this.setState({ updatesEnabled: false })
      }
  }

export const setLocation = location => {
  return {
    type: SET_LOCATION,
    location: location
  }
}