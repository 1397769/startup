
// thunk functions need dispatch, getState
// disatch: can dispatch new actions
// getstate: can acces de the current state
import { SET_PRODUCTS, ADD_SUBS, DELETE_SUBS } from "./actionTypes";

export const getProducts = (filter) => {
    return (dispatch, getState) => {
        let authToken =getState().auth.idToken;
        fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/product', 
        {
            method: "POST",
            body: JSON.stringify(filter),
            headers: {
                Authorization: "Bearer " + authToken,
                "Content-Type": 'application/json'
            } 
        }).catch(err => {
            console.log(err);
            // TO DO son productos
            alert('Error en el proceso de obtención de productos');
        })
        .then(res => res.json())
        .then((productsData)=> {
           productsData.find((product)=> {
                var isSubs =getState().userProducts.userProducts.findIndex((myProduct)=> {
                    var idProduct =Object.keys(product);
                    if(myProduct.idProduct == idProduct) {
                        return true;
                    } else { 
                        return false;
                    }
                });
            if (isSubs >= 0) {
                product[Object.keys(product)].isSubs = true;

            } else {
                product[Object.keys(product)].isSubs = false;
            }
            });
            dispatch(setProducts(productsData));
        });

    }
}
// action creator
// plain object and type property
export const setProducts = products => {
    return {
        type: SET_PRODUCTS,
        products: products
    };
};

export const addSubs = (idProduct)=> {
    return (dispatch) => {
         dispatch(setSubs(idProduct));
    }
};

export const setSubs = idProduct => {
    return {
        type: ADD_SUBS,
        idProduct: idProduct
    };
};
export const deleteSubs = (idProduct)=> {
    return (dispatch) => {
         dispatch(unsetSubs(idProduct));
    }
};
export const unsetSubs = idProduct => {
    return {
        type: DELETE_SUBS,
        idProduct: idProduct
    };
};