
// thunk functions need dispatch, getState
// disatch: can dispatch new actions
// getstate: can acces de the current state
import { SET_SHOPS } from "./actionTypes";
import { NOT_NETWORK } from "../../constants";

export const getShops = (filter) => {
    return (dispatch, getState) => {
        const latlong = [getState().location.location.coords.latitude, getState().location.location.coords.longitude];
        //var latlong = [41.381722,  2.172542];
        filter.position = latlong;
        filter.radius = filter.distance;
        let authToken =getState().auth.idToken;
        fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/shops', 
        {
            method: "POST",
            body: JSON.stringify(filter),
            headers: {
                Authorization: "Bearer " + authToken,
                "Content-Type": 'application/json'
            } 
        }).catch(err => {
            console.log(err);
            if(err.message = NOT_NETWORK) {
                alert('No tienes internet. Comprueba la conexión');
            } else {
                alert('Error en el proceso de obtención de tiendas');
            }
        })
        .then(res => res.json())
        .then((shopsData)=> {
            console.log('Datos recibidos: ', shopsData);
            dispatch(setShops(shopsData));
        });

    }
}
// action creator
// plain object and type property
export const setShops = shops => {
    return {
        type: SET_SHOPS,
        shops: shops
    };
};


