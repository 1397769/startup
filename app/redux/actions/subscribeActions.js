
// thunk functions need dispatch, getState
// disatch: can dispatch new actions
// getstate: can acces de the current state
import {AsyncStorage} from 'react-native';
import { NOT_NETWORK } from "../../constants";

getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('auth:idToken');
      return value;
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
};

export const addSubscribe =async (product, productData) => {
    const authToken = await getToken();
    console.log('Muestro subscribe product', product);
        fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/subscribe', 
        {
            method: "POST",
            body: JSON.stringify(product),
            headers: {
                Authorization: "Bearer " + authToken,
                "Content-Type": 'application/json'
            } 
        }).catch(err => {
            if(err.message = NOT_NETWORK) {
                alert('No tienes internet. Comprueba la conexión');
            } else {
                alert('Error en el proceso de añadir producto');
            }
        })
        .then(res => res.json())
        .then((productData)=> {
            // TO DO guardar en async los datos del producto subscrito
            console.log('Datos recibidos una vez suscrito: ', productData);
        });
};
export const removeSubscribe = async (product) => {
        const authToken = await getToken();
        console.log('Muestro baja de product', product);
        fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/unsubscribe', 
        {
            method: "POST",
            body: JSON.stringify(product),
            headers: {
                Authorization: "Bearer " + authToken,
                "Content-Type": 'application/json'
            } 
        }).catch(err => {
            if(err.message = NOT_NETWORK) {
                alert('No tienes internet. Comprueba la conexión');
            } else {
                alert('Error en el proceso de eliminar producto');
            }
        })
        .then(res => res.json())
        .then((productData)=> {
            // TO DO guardar en async los datos del producto subscrito
            console.log('Datos recibidos una vez desuscrito: ', productData);
        });

    
}

//  https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export