
// thunk functions need dispatch, getState
// disatch: can dispatch new actions
// getstate: can acces de the current state
import { SET_NOTIFICATIONS, SET_USER_PRODUCTS, ADD_USER_PRODUCT, DELETE_USER_PRODUCT } from "./actionTypes";
import { NOT_NETWORK } from "../../constants";
import { AsyncStorage } from 'react-native';


export const getUserProducts = (filter) => {
    return (dispatch, getState) => {
        let authToken =getState().auth.idToken;
        fetch('https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/userProducts', 
        {
            method: "GET",
            headers: {
                Authorization: "Bearer " + authToken,
            } 
        }).catch(err => {
            if(err.message = NOT_NETWORK) {
                alert('No tienes internet. Comprueba la conexión');
            } else {
                alert('Error en el proceso de añadir producto');
            }
        })
        .then(res => res.json())
        .then((productsData)=> {
            console.log('Datos recibidos: ', productsData);
            dispatch(setUserProducts(productsData));
        });

    }
}
// action creator
// plain object and type property
getLocalNotifications = async () => {
    try {
      const value = await AsyncStorage.getItem('notifications');
      return JSON.parse(value);
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
};
export const getNotifications =  ()=> {
    return async dispatch => {
        const notifications = await getLocalNotifications();
         dispatch(setNotifications(notifications));
    }
};

export const setNotifications = notifications => {
    return {
        type: SET_NOTIFICATIONS,
        notifications: notifications
    };
};

export const setUserProducts = products => {
    return {
        type: SET_USER_PRODUCTS,
        userProducts: products
    };
};
export const addUserProduct = (userProduct)=> {
    return (dispatch) => {
         dispatch(addUserProductAction(userProduct));
    }
};
export const addUserProductAction = userProduct => {
    return {
        type: ADD_USER_PRODUCT,
        userProduct: userProduct
    };
};
export const deleteUserProduct = (userProduct)=> {
    return (dispatch) => {
         dispatch(deleteUserProductAction(userProduct));
    }
};
export const deleteUserProductAction = idProduct => {
    return {
        type: DELETE_USER_PRODUCT,
        idProduct: idProduct
    };
};