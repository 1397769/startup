import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import shopsReducer from "./reducers/shopReducer";
import locationReducer from "./reducers/locationReducer";
import productReducer from "./reducers/productReducer";
import authReducer from "./reducers/authReducer";
import userProductsReducer from "./reducers/userProductsReducer";

// import uiReducer from "./reducers/ui";
// import authReducer from "./reducers/auth";
//   auth: authReducer
const rootReducer = combineReducers({
  shops: shopsReducer,
  location: locationReducer,
  products: productReducer,
  auth: authReducer,
  userProducts: userProductsReducer,
});

let composeEnhancers = compose;

if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
  return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
};

export default configureStore;
