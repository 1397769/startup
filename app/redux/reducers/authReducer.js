import { SET_TOKEN, DELETE_TOKEN } from "../actions/actionTypes";

const initialState = {
  idToken: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        idToken: action.idToken,
      };
      case DELETE_TOKEN:
        return {
          ...state,
          idToken: false,
        };
    default:
      return state;
  }
};

export default reducer;
