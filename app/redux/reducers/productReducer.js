import { SET_PRODUCTS, ADD_SUBS, DELETE_SUBS } from "../actions/actionTypes";

const initialState = {
  products: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        ...state,
        products: action.products
      };
    case ADD_SUBS:
        var updatedProducts = state.products.filter(element => {
          if (Object.keys(element)[0] === action.idProduct[0]) {
            element[Object.keys(element)].isSubs = true;
          }
          return true;
        });
        return {
          ...state,
          products: updatedProducts
        };
    case DELETE_SUBS:
          var updatedProducts = state.products.filter(element => {
            if (Object.keys(element)[0] === action.idProduct[0]) {
              element[Object.keys(element)].isSubs = false;
            }
            return true;
          });
          return {
            ...state,
            products: updatedProducts
        };
    default:
      return state;
  }
};

export default reducer;
