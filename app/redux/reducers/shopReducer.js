import { SET_SHOPS } from "../actions/actionTypes";

const initialState = {
  shops: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOPS:
      return {
        ...state,
        shops: action.shops
      };
    default:
      return state;
  }
};

export default reducer;
