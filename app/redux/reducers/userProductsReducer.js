import {  SET_NOTIFICATIONS, SET_USER_PRODUCTS, ADD_USER_PRODUCT, DELETE_USER_PRODUCT } from "../actions/actionTypes";

const initialState = {
  userProducts: [],
  notifications: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_PRODUCTS:
      return {
        ...state,
        userProducts: action.userProducts
      };
    case ADD_USER_PRODUCT:
        if (state.userProducts && state.userProducts.length > 0){
          return { 
            ...state,
            userProducts: [...state.userProducts, action.userProduct]
          };
        } else {
          return {
            ...state,
            userProducts: [action.userProduct]
          };

        };
      
    case DELETE_USER_PRODUCT:
          var updatedProducts = state.userProducts.filter(element => {
            return (Object.keys(element)[0] !== action.idProduct);
          });
          return {
            ...state,
            userProducts: updatedProducts
        };
    case SET_NOTIFICATIONS:
        return {
          ...state,
          notifications: action.notifications
        };
    default:
      return state;
  }
};

export default reducer;
