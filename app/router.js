import React from 'react';
import SignUpView from "./views/SignUpView";
import ShopListView from "./views/ShopListView"
import LinksScreen from "./views/LinksScreen";
import { createBottomTabNavigator, createAppContainer, createSwitchNavigator,createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import LoginView from './views/LoginView';
import FilterView from './views/FilterView';
import ProductListView from "./views/ProductListView"
import ProductWebView from './views/ProductWebView';
import AuthLoadingScreen from './views/AuthLoadingScreen';
import NotificationView from './views/NotificationView';
import {Icon, Image} from 'react-native';


export const AuthNavigator = createStackNavigator({
    Login: {
        screen: LoginView,
        navigationOptions: ({navigation}) => ({
          header: null,
        }),
    },
    SignUp: {
        screen: SignUpView,
    },
});

export const ListsNavigator = createStackNavigator({
  ShopListView: {
    screen: ShopListView,
    navigationOptions: ({navigation}) => ({
      header: null,
    }),
},
ProductListView: {
  screen: ProductListView
},
WebProduct: {
  screen: ProductWebView,
},

})
export const MainNavigator = createStackNavigator({
  Filter: {
    screen: FilterView,
    navigationOptions: {
      title: "FILTRAR",
    }
  },
  ListsNavigator: {
    screen: ListsNavigator,
    navigationOptions: ({navigation}) => ({
      header: null,
    }),
},
})
export const SuscriptionNavigator = createStackNavigator({
  Suscription: {
    screen: LinksScreen,
    navigationOptions: ({navigation}) => ({
      header: null,
    }),
},
  WebProduct: {
    screen: ProductWebView,
  },
})
export const TabTopNavigator = createMaterialTopTabNavigator({
  NotificationTab: {
      screen: NotificationView,
      navigationOptions: {
        title: "Notificaciones",
      }
  },
  SusriptionTab: { screen: SuscriptionNavigator,
    navigationOptions: {
      title: "Suscripción",
    }},
}, {
  tabBarOptions: {
    style: {    
      backgroundColor: '#841584',
    },
    indicatorStyle: {
      paddingVertical: 2,
      backgroundColor: 'black',
  }
  }
})
export const TabNavigator = createBottomTabNavigator({
    Main: {
        screen: MainNavigator,
        navigationOptions: {
          title: "DESCUBRE",
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
            if(focused) {
              return (
                <Image
                  source={ require('./assets/onDiscover.png') }
                  style={{ width: 25, height: 25, }} />
              );
            } else {
              return (
                <Image
                  source={ require('./assets/discover.png') }
                  style={{ width: 20, height: 20, }} />
              );
            }
            
            } 
          }
    },
    ProfileTab: { screen: TabTopNavigator,
      navigationOptions: {
        title: "MI CUENTA",
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
          if(focused) {
            return (
              <Image
                source={ require('./assets/onProfile.png') }
                style={{ width: 25, height: 25, }} />
            );
          } else {
            return (
              <Image
                source={ require('./assets/profile.png') }
                style={{ width: 20, height: 20, }} />
            );
          }
        } 
      },
    },
      
  
})

export const createRootNavigator = (signedIn = false) => {
    const RootNavigator = createSwitchNavigator(
      {   
        AuthLoading: AuthLoadingScreen,

        AuthNavigator: {
          screen: AuthNavigator
        },
        TabNavigator: {
          screen: TabNavigator
        }
      },
      {
        initialRouteName:  "AuthLoading",
      }
    );
    const NavigatorContainer =  createAppContainer(RootNavigator);
    return NavigatorContainer;
  };