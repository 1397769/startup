export async function getShops() {
    try {
        let url = 'https://us-central1-testtfg-57399.cloudfunctions.net/addBrand/info';
        let response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({keyUser: 'idJuan'})
        });
        let responseJson = await response.json();
        return responseJson;
    } catch (error) {
        console.error(error);
    }
}