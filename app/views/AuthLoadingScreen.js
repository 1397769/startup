import React from 'react';
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { setToken } from "../redux/actions/indexActions";
import { connect } from "react-redux";



class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('auth:idToken');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    if(userToken) {
      this.props.onSetToken(userToken);
      this.props.navigation.navigate('TabNavigator');

    } else {
      this.props.navigation.navigate('AuthNavigator');
    }

  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const mapDispatchToProps = dispatch => {
  return {
    onSetToken: (userToken) => dispatch(setToken(userToken)),
  };
};
export default connect(null, mapDispatchToProps)(AuthLoadingScreen);