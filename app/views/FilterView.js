import React from 'react';
import { Text, View, Picker, StyleSheet, Button } from 'react-native';
import SwitchSelector from "react-native-switch-selector";
import Slider from '@react-native-community/slider';
import { connect } from "react-redux";
import { getShops, getProducts, getUserProducts, getLocation } from "../redux/actions/indexActions";
import {ToastAndroid} from 'react-native';
import ButtonFilter from "../components/UI/ButtonFilter/ButtonFilter";


class FilterView extends React.Component {
    state = {
        idBrand: '',
        gender: 'f',
        category: '',
        maximumPrice: 200,
        colour: '',
        distance: 2.00
    }
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.onGetUserProducts();
        this.props.onGetLocationUser();
    }
    render() {
        var filter = {};
        let shopTitle = null
        let distanceField = null;
        let buttonFilter = null;
        let distanceFieldValue =
            <View style={styles.containerBlockSlider}>
                <Text>Distancia</Text>
                    <View style={styles.containerSlider}>
                        <Slider
                            style={{ width: 300, height: 40 }}
                            minimumValue={0.01}
                            maximumValue={2}
                            value={5}
                            minimumTrackTintColor="#20b2aa"
                            maximumTrackTintColor="#000000"
                            onValueChange={value => this.setState({ distance: value })}
                        />
                        <View style={styles.textLimits}>
                            <Text style={styles.minSlider}>0 km</Text>
                            <Text style={styles.textSlider}>{parseFloat(this.state.distance).toFixed(2)} km</Text>
                            <Text style={styles.maxSlider}>2 km</Text>
                        </View>
                    </View>
            </View>;
        if (this.props.navigation.state.params && this.props.navigation.state.params.idBrand) {
            // escribir aqui titulo
            shopTitle = <Text>
                FILTRAR PRODUCTOS
            </Text>;
            buttonFilter = <ButtonFilter
                onPress={() => {
                    if(this.props.navigation.state.params && this.props.navigation.state.params.idBrand) 
                    {
                        filter.idBrand = this.props.navigation.state.params.idBrand;
                        filter.gender = this.state.gender;
                        filter.category = this.state.category;
                        filter.maximumPrice = this.state.maximumPrice;
                        filter.colour = this.state.colour;
                        filter.distance = this.state.distance;
                        this.props.onGetProducts(filter);
                        this.props.navigation.navigate("ProductListView", filter)
                    } else {
                        ToastAndroid.showWithGravityAndOffset(
                        'Seleccione categoría',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                        );
                    }
                }}
                color="#841584">
                Aplicar
            </ButtonFilter>;
        } else {
            distanceField = distanceFieldValue;
            buttonFilter = <ButtonFilter
                onPress={() => {
                    if(this.state.category && this.state.category.length >1){
                    filter.gender = this.state.gender;
                    filter.category = this.state.category;
                    filter.maximumPrice = this.state.maximumPrice;
                    filter.colour = this.state.colour;
                    filter.distance = this.state.distance;
                    this.props.onGetShops(filter);
                    this.props.navigation.navigate("ShopListView", filter)
                    } else{
                        ToastAndroid.showWithGravityAndOffset(
                        'Seleccione categoría',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                        );
                    }

                }}
                color="#841584">
                Aplicar
                </ButtonFilter>;
        };
        const categories = [{ 'categoryName': 'Seleccione', 'categoryValue': '' },
        { 'categoryName': 'Pantalón', 'categoryValue': 'trousers' },
        {'categoryName': 'Calzado', 'categoryValue': 'shoes' },
        {'categoryName': 'Chaquetas', 'categoryValue': 'jackets' },
        {'categoryName': 'Sudadera', 'categoryValue': 'sweater' }];

        const colours = [{ 'colourName': 'Todos', 'colourValue': '' },
        { 'colourName': 'Negro', 'colourValue': 'dark' },
        { 'colourName': 'Blanco', 'colourValue': 'white' }];

        const options = [
            { label: "Mujer", value: "f" },
            { label: "Hombre", value: "m" }
        ];

        return (
            <View style={styles.container}>
                <View>
                    {shopTitle}
                    <SwitchSelector
                        initial={0}
                        onPress={value => this.setState({ gender: value })}
                        options={options}
                        buttonColor={'#841584'}
                        selectedColor={'#ffffff'}
                        borderColor={'#841584'}
                        textColor={'#841584'}
                        hasPadding
                    />
                </View>
                <View>
                    <Text>Categoría*</Text>
                    <Picker
                        style={styles.pickerStyle}
                        selectedValue={this.state.category}
                        onValueChange={(type) => this.setState({ category: type })}>{
                            categories.map((item) => {
                                return <Picker.Item label={item.categoryName} value={item.categoryValue} />
                            })
                        }
                    </Picker>
                    <View style={styles.separator} />
                </View>
                <View>
                    <Text>Color</Text>
                    <Picker
                        style={styles.pickerStyle}
                        selectedValue={this.state.colour}
                        onValueChange={(colour) => this.setState({ colour: colour })}>{
                            colours.map((item) => {
                                return <Picker.Item label={item.colourName} value={item.colourValue} />
                            })
                        }
                    </Picker>
                    <View style={styles.separator} />
                </View>
                <View style={styles.containerBlockSlider}>
                    <Text>Precio</Text>
                    <View style={styles.containerSlider} >
                        <Slider
                            style={{ width: 300, height: 40 }}
                            minimumValue={1}
                            maximumValue={200}
                            value={200}
                            minimumTrackTintColor="#20b2aa"
                            maximumTrackTintColor="#000000"
                            onValueChange={value => this.setState({ maximumPrice: value })}
                        />
                        <View style={styles.textLimits}>
                            <Text style={styles.minSlider}>0 €</Text>
                            <Text style={styles.textSlider}>{parseFloat(this.state.maximumPrice).toFixed(2)} €</Text>
                            <Text style={styles.maxSlider}> 200 €</Text>
                        </View>
                    </View>
                </View>
                {distanceField}
                {buttonFilter}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pickerStyle: {
        width: '100%'
    },
    containerBlockSlider: {
        flex: 1,
        flexDirection: 'column',
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 25,
    },
    textSlider: {
        fontSize: 14,
        fontWeight: '500',
    },
    minSlider: {
        fontSize: 14,
        fontWeight: '500',
    },
    maxSlider: {
        fontSize: 14,
        fontWeight: '500',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    },
    separator: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        marginBottom: 20,
    },
    containerCenter: {
        alignItems: 'center',
    },
    containerSlider: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textLimits: {
        flex: 1,
        width: 310,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});
const mapDispatchToProps = dispatch => {
    return {
        onGetShops: (filter)=> dispatch(getShops(filter)),
        onGetProducts: (filter)=> dispatch(getProducts(filter)),
        onGetUserProducts: () => dispatch(getUserProducts()),
        onGetLocationUser: () => dispatch(getLocation())
    };
};
export default connect(null, mapDispatchToProps)(FilterView);
