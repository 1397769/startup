import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import SavedList from "../components/SavedList/SavedList";
import { getUserProducts, deleteUserProduct, deleteSubs } from '../redux/actions/indexActions';
import { connect } from "react-redux";
import { removeSubscribe } from '../redux/actions/subscribeActions';
import { storeToken, deleteToken } from '../redux/actions/auth';
import { withNavigation } from "react-navigation";
import ButtonForm from "../components/UI/ButtonForm/ButtonForm";




class LinksScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }
  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", ()=> {
      this.props.onGetUserProducts();
    })
    }

  componentWillUnmounted() {
    // Remove the event listener
    this.focusListener.remove();
  }
  logOutHandler = async () => {
    await this.props.onDeleteToken();
    if(!this.props.idToken) {
      alert('Has cerrado sesión');
      this.props.navigation.navigate('AuthNavigator');
    } else {
      alert('Intentalo de luego, más tarde');
    }
   }

  itemSelectedHandler = key => {
    this.props.navigation.push("WebProduct", key);
  };
  subscribeSelectedHandler = key => {
    removeSubscribe(key);
    this.props.onDeleteSubs(key.idProduct);
    this.props.onDeleteUserProduct(key.idProduct);
    this.setState(previousState=> (
    {
      isLoading: !previousState.isLoading
    }));
};

  render() {
    if (!this.state.isLoading) {
      console.log(this.state.dataSource);
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <Text>
            {this.state.dataSource}
          </Text>
        </View>
      )
    }
    return (
      <View style={{flex:1}}>
      <View>
        </View>
        <View style={{flex:1}}>
          <SavedList
            products={this.props.products}
            onSubscribeSelected= {this.subscribeSelectedHandler}
            onItemSelected={this.itemSelectedHandler}
            logOut={this.logOutHandler}
          />
        </View>
        <View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "orange",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "orange",
    fontWeight: "bold",
    fontSize: 26
  }
});

const mapStateToProps = state => {
    return {
      products: state.userProducts.userProducts,
      idToken: state.auth.idToken,
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onGetUserProducts: () => dispatch(getUserProducts()),
      onDeleteSubs: (idProduct) => dispatch(deleteSubs(idProduct)),
      onDeleteToken: () => dispatch(deleteToken()),
      onDeleteUserProduct: (idProduct)=> dispatch(deleteUserProduct(idProduct))
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(LinksScreen);