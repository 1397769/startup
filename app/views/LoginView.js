import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
} from 'react-native';
import { connect } from "react-redux";
import { StackActions, NavigationActions } from 'react-navigation';
import { getLocation } from "../redux/actions/locationActions";



import { MonoText } from '../components/StyledText';
import ButtonForm from "../components/UI/ButtonForm/ButtonForm";
import DefaultInput from "../components/UI/DefaultInput/DefaultInput";
import HeadingText from "../components/UI/HeadingText/HeadingText";
import  {authLogin}  from '../redux/actions/auth';
// import { setAuth } from '../redux/actions/auth';

class LoginView extends React.Component {
  constructor(props) {
    super(props);
    }
  state = {
    authData: {
      email: {
        value: '',
        valid: false,
      },
      password: {
        value: '',
        valid: false,
      },
    }
  };
  componentDidMount(){
    this.props.onGetLocationUser();
  }
  userAuthDataHandler = () => {
  const userAuthData = {
    email: this.state.authData.email.value,
   password: this.state.authData.password.value
 };  
 this.props.handlerAuthLogin(userAuthData);
 };
  
 changeInputState = (key, value) => {
  this.setState(prevState => {
    return {
      authData: {
        ...prevState.authData,
        [key]: {
          ...prevState.authData[key],
          value: value,
        }
      }
    };
  });
};
  render() {
    const resetAction = StackActions.reset({
      index: 0,
      key: undefined,
      actions: [
        NavigationActions.navigate({
          routeName: "MainNavigator"
        }), NavigationActions.navigate({ routeName:"Filter" })
      ]
    });
    if(this.props && this.props.idToken) {
     // this.props.navigation.dispatch(resetAction);
       this.props.navigation.navigate('Filter');
    }
    return (
      <View style={styles.container}>
        <View style={styles.welcomeContainer}>
          <HeadingText>Iniciar sesión</HeadingText>
        </View>
        <View style={styles.inputContainer}>
          <DefaultInput placeholder="Email"             
          onChangeText={data => this.changeInputState('email', data)}/>
          <DefaultInput placeholder="Contraseña"
          secureTextEntry={true} 
          onChangeText={data => this.changeInputState('password', data)} />
          <ButtonForm color="#29aaf4" onPress={this.userAuthDataHandler}>
            Entrar
            </ButtonForm>
            <ButtonForm color="#29aaf4" onPress={() => this.props.navigation.navigate("SignUp")}>
            Registrate
            </ButtonForm>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    marginTop: '20%',
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "80%"
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    width: "80%"
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
const mapStateToProps = state => {
  return {
    idToken: state.auth.idToken,
    location: state.location.location 
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handlerAuthLogin: (userAuthData) => dispatch(authLogin(userAuthData, this.props)),
    onGetLocationUser: () => dispatch(getLocation())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
