import React from 'react';
import { Button, View, StyleSheet, Text } from 'react-native';
import NotificationList from "../components/NotificationList/NotificationList";
import { getUserProducts } from '../redux/actions/indexActions';
import { connect } from "react-redux";
import { addSubscribe } from '../redux/actions/subscribeActions';
import { getNotifications } from '../redux/actions/userProductAction';


class NotificationView extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }
  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", ()=> {
      this.props.onGetNotifications();
    })
    }

  componentWillUnmounted() {
    // Remove the event listener
    this.focusListener.remove();
  }

  itemSelectedHandler = key => {
    this.props.navigation.push("WebProduct", key);
  };
  subscribeSelectedHandler = key => {
    addSubscribe(key);
};
  render() {
    var isElement = null;
    if (!this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <Text>
            {this.state.dataSource}
          </Text>
        </View>
      )
    }
    if (this.props.products == null) {
      isElement = <Text style = {styles.noResult}> No tienes ninguna notificación
      </Text>;
    }
    return (
        <View style={{flex:1}}>
          {isElement}
          <NotificationList
            products={this.props.products}
            onSubscribeSelected= {this.subscribeSelectedHandler}
            onItemSelected={this.itemSelectedHandler}
          />
        </View>
    );
  }
}
const styles = StyleSheet.create({
  noResult: {
    fontWeight: 'bold',
    fontSize: 20
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "orange",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "orange",
    fontWeight: "bold",
    fontSize: 26
  }
});

const mapStateToProps = state => {
    return {
      products: state.userProducts.notifications
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onGetNotifications: () => dispatch(getNotifications())
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(NotificationView);