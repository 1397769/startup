import React from 'react';
import { Button, View, StyleSheet, Text } from 'react-native';
import ProductList from "../components/ProductList/ProductList";
import { getProducts, addSubs, deleteSubs, addUserProduct, deleteUserProduct} from '../redux/actions/indexActions';
import { connect } from "react-redux";
import { addSubscribe, removeSubscribe } from '../redux/actions/subscribeActions';
import { storeToken } from '../redux/actions/auth';
import ButtonFilter from "../components/UI/ButtonFilter/ButtonFilter";



class ProductListView extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }
  componentDidMount() {
    this.props.onGetProducts(this.props.navigation.state.params);
    this.setState(previousState=> (
      {
        isLoading: !previousState.isLoading
      }));
    }

  itemSelectedHandler = key => {
      this.props.navigation.push("WebProduct", key[Object.keys(key)]);
  };
  subscribeSelectedHandler = key => {
    var productPath = {};
    productPath.idBrand = this.props.navigation.state.params.idBrand;
    productPath.category = this.props.navigation.state.params.category;
    productPath.idProduct = Object.keys(key);
    productPath.productData = key[Object.keys(key)];
    key[productPath.idProduct].isSubs = !(key[productPath.idProduct].isSubs);
      if (key[productPath.idProduct].isSubs) {
        addSubscribe(productPath);
        var subsObject = Object.assign({},key[productPath.idProduct]);
        subsObject.idProduct = productPath.idProduct[0];
        subsObject.idBrand = productPath.idBrand;
        this.props.onAddUserProduct(subsObject);
        this.props.onAddSubs(productPath.idProduct);
      } else {
        removeSubscribe(productPath);
        this.props.onDeleteSubs(productPath.idProduct);
        this.props.onDeleteUserProduct(productPath.idProduct);
      }
      this.setState(previousState=> (
        {
          isLoading: !previousState.isLoading
        }));
};
  render() {
    // en caso de que este cargado
    if (false) {
      console.log(this.state.dataSource);
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <Text>
            {this.state.dataSource}
          </Text>
        </View>
      )
    }
    return (
      <View style={{flex:1}}>
      <View style={styles.transparent}>
      <ButtonFilter color="#841584" onPress={() => this.props.navigation.push("Filter", this.props.navigation.state.params)}>
            Filtrar
          </ButtonFilter>
        </View>
        <View style={{flex:1}}>
          <ProductList
            extraData= {this.state.isLoading}
            products={this.props.products}
            onSubscribeSelected= {this.subscribeSelectedHandler}
            onItemSelected={this.itemSelectedHandler}
          />
        </View>
        <View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  transparent: {
    backgroundColor:'transparent'
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "orange",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "orange",
    fontWeight: "bold",
    fontSize: 26
  }
});

const mapStateToProps = state => {
    return {
      products: state.products.products,
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onGetProducts: (filter) => dispatch(getProducts(filter)),
      onAddSubs: (idProduct) => dispatch(addSubs(idProduct)),
      onDeleteSubs: (idProduct) => dispatch(deleteSubs(idProduct)),
      onAddUserProduct: (userProduct)=> dispatch(addUserProduct(userProduct)),
      onDeleteUserProduct: (idProduct)=> dispatch(deleteUserProduct(idProduct))
    };
  };
  export default connect(mapStateToProps, mapDispatchToProps)(ProductListView);