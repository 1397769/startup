import React from 'react';
import { View, StyleSheet  } from 'react-native';
import ItemWeb from "../components/UI/ItemWeb/ItemWeb";

export default class ProductWebView extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <ItemWeb url={this.props.navigation.state.params.url} />
        );
    }
}

