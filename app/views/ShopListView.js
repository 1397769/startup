import React from 'react';
import { Button, View, StyleSheet, Text } from 'react-native';
import ShopList from "../components/ShopList/ShopList";
import { connect } from "react-redux";
import { getShops } from "../redux/actions/indexActions";
import { getLocationUpdates } from "../redux/actions/locationActions";
import AsyncStorage from '@react-native-community/async-storage';
import ButtonFilter from "../components/UI/ButtonFilter/ButtonFilter";


var filter = {};

class ShopListView extends React.Component {
  // static navigationOptions = {
  // title: 'Settings',
  // };
  constructor(props) {
    super(props);
    this.state = { isLoading: true }
    _bootstrapAsync = async () => {
      const userToken = await AsyncStorage.getItem('auth:idToken');
  
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      console.log('Init navigation: ',userToken);
    };
  }
  componentDidMount() {
    var filter = {};
    if (this.props.navigation.state.params) {
      filter = this.props.navigation.state.params;
      filter.position = this.props.location;
      this.props.onGetShops(filter);
    } else {
       filter = {
        idBrand: '',
        gender: '',
        category: '',
        maximumPrice: 200,
        colour: '',
        radius: 5
      };
    }
    this.props.onLocationUser(filter);


  }
    itemSelectedHandler = key => {
       var aux = this.props.navigation.state.params;
       aux['idBrand'] = key.idBrand;
      this.props.navigation.navigate("ProductListView", aux)

  };
  render() {
    if (!this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <Text>
            {this.state.dataSource}
          </Text>
        </View>
      )
    }
    return (
      <View style={{flex:1}}>
        <View >
          <ButtonFilter color="#841584" onPress={() => this.props.navigation.push("Filter")}>
            Filtrar
          </ButtonFilter>
        </View>
        <View style={{flex:1}}>
          <ShopList
            shops={this.props.shops}
            onItemSelected={this.itemSelectedHandler}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "orange",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "orange",
    fontWeight: "bold",
    fontSize: 26
  }
});
const mapStateToProps = state => {
  return {
    shops: state.shops.shops,
    location: state.location.location
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLocationUser: (filter) => dispatch(getLocationUpdates(filter)),
    onGetShops: (filter) => dispatch(getShops(filter))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ShopListView);

