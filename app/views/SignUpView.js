import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  ToastAndroid
} from 'react-native';

import { MonoText } from '../components/StyledText';
import ButtonForm from "../components/UI/ButtonForm/ButtonForm";
import DefaultInput from "../components/UI/DefaultInput/DefaultInput";
import HeadingText from "../components/UI/HeadingText/HeadingText";
import validationInputs from "../modules/validationInputs";
import  {authSignUp}  from '../redux/actions/auth';

// import { setAuth } from '../redux/actions/auth';

export default class LoginView extends React.Component {
   userAuthDataHandler = () => {
     if (!this.state.authData.confirmPassword.valid ||
      !this.state.authData.email.valid ||
      !this.state.authData.password.valid){
        if(!this.state.authData.email.valid) {
          ToastAndroid.show('Email no valido', ToastAndroid.LONG);
        } else {
          if(!this.state.authData.password.valid) {
            ToastAndroid.show('La contraseña debe superar los 6 digitos', ToastAndroid.LONG);
          } else {
            if(!this.state.authData.confirmPassword.valid){
              ToastAndroid.show('Las contraseñas no coinciden', ToastAndroid.LONG);
            }
          }
        }
      } else {
        const userAuthData = {
          email: this.state.authData.email.value,
         password: this.state.authData.password.value
       };
       authSignUp(userAuthData);
      }
  }
  state = {
    authData: {
      email: {
        value: '',
        valid: false,
        validation: {
          isEmail: true
        }
      },
      password: {
        value: '',
        valid: false,
        validation:{
          minLength: 6
        }
      },
      confirmPassword: {
        value: '',
        valid: false,
        validation: {
          equalTo: 'password'
        }
      }
    }
  };

  
  changeInputState = (key, value) => {
    let compareValues = {};
    if (this.state.authData[key].validation.equalTo) {
      const equalControl = this.state.authData[key].validation.equalTo;
      const equalValue = this.state.authData[equalControl].value;
      compareValues = {
        ...compareValues,
        equalTo: equalValue
      };
    }
    if (key === "password") {
      compareValues = {
        ...compareValues,
        equalTo: value
      };
    }
    this.setState(prevState => {
      return {
        authData: {
          ...prevState.authData,
          confirmPassword: {
            ...prevState.authData.confirmPassword,
            valid:
              key === "password"
                ? validationInputs(
                    prevState.authData.confirmPassword.value,
                    prevState.authData.confirmPassword.validation,
                    compareValues
                  )
                : prevState.authData.confirmPassword.valid
          },
          [key]: {
            ...prevState.authData[key],
            value: value,
            valid: validationInputs(
              value,
              prevState.authData[key].validation,
              compareValues
            )
          }
        }
      };
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.welcomeContainer}>
          <HeadingText>Registrate</HeadingText>
        </View>
        <View style={styles.inputContainer}>
          <DefaultInput
            placeholder="Email"
            onChangeText={data => this.changeInputState('email', data)}
          />
          <DefaultInput placeholder="Contraseña"
            secureTextEntry={true} 
            onChangeText={data => this.changeInputState('password', data)}
          />
          <DefaultInput placeholder="Repite contraseña"
            secureTextEntry={true} 
            onChangeText={data => this.changeInputState('confirmPassword', data)}
          />
          <ButtonForm color="#29aaf4" onPress={this.userAuthDataHandler}>
            Entrar
            </ButtonForm>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    marginTop: '20%',
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "80%"
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    width: "80%"
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
